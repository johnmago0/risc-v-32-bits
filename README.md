# RISC-V 32 bits

Hello there this is a implementation of a RISC-V architecture, made in logisim-evolution version 3.8.0. 

This implementation is only possible because of the @tkkarchive, so all kudos to him. The video for his implementation can be found in his channel on the following link: <https://www.youtube.com/watch?v=kbyIHfqmrCw>.

##### TODO
- [ ] Better descriptions
- [ ] Add Documentation of Parts of the CPU
